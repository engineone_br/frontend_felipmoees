<?php
session_start();
include("config/header.php");
$sql="SELECT p.id_pubs,p.usuario,p.texto,p.data,u.id,u.usuario,u.img FROM `tb_pubs` as p 
INNER JOIN tb_user as u on p.usuario=u.id  where id_pubs = ".base64_decode($_GET['code']);
$res=mysqli_query($linkdeconexao_db,$sql);
?>
<!doctype html>
<html lang="pt-br">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">


<link rel="icon" href="img/icon.png">
<title>PrivêWS</title>

<!-- Principal CSS do Bootstrap -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">

<!-- Estilos customizados -->
<link href="dist/css/dashboard.css" rel="stylesheet">
<link href="dist/css/theme.css" rel="stylesheet">
<link href="dist/css/jquery.privews.min.css" rel="stylesheet">
<link href="fancybox/jquery.fancybox.min.css" rel="stylesheet">
<link href="select2/select2.min.css" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.form.min.js"></script>
    <script type="text/javascript">
    curtir = function(x)
    {
            var valor = $(x).attr('value');
            var url='/curtir.php?id_post='+valor;
            $.ajax({
    		    type:'post', //Definimos o método HTTP usado   //Definimos o tipo de retorno
    		    url: url,   //Definindo o arquivo onde serão buscados os dados
    		    success: function(dados)
    		    {
    		        $(x).empty();
    		        $(x).append(dados);
    		    }
    	    });
    }
        $(document).ready(function () {
    $('#submitComentario').click(function () {
            $('#CForm').ajaxForm({
                target: '#outputComentario',
                url: 'comentar.php',
                beforeSubmit: function () 
                {
                    $("#progressDivId").css("display", "block");
                    var percentValue = '0%';
                    $('#progressBar').width(percentValue);
                    $('#percent').html(percentValue);
                },
                uploadProgress: function (event, position, total, percentComplete) {

                    
                },
                error: function (response, status, e) {
                    alert('Erro.');
                },
                complete: function (xhr) {
                    console.log("resposta:"+xhr.responseText);
                    if (xhr.responseText != "error")
                    {
                          $("#comentario_ok").html(xhr.responseText);
                          $("#progressDivId").css("display", "none");
                    }
                    else if (xhr.responseText == "error")
                    {  
                          $("#outputImage").html(xhr.responseText);
                           $("#outputImage").show();
                           $("#outputImage").html("<div class='error'>Problem in uploading file.</div>");
                           $("#progressDivId").css("display", "none");
                    }
                    else
                    {
                         alert("Erro");
                    }
                }
            });
    });
});
    comentario = function(btn)
    {
        alert("ok");
        var valor=<?php echo"'".$row['id_pubs']."'"; ?>;
        var url='/atualizar_comentario.php?id='+valor
            $.ajax({
    		    type:'get', //Definimos o método HTTP usado   //Definimos o tipo de retorno
    		    url: url,   //Definindo o arquivo onde serão buscados os dados
    		    success: function(dados)
    		    {
    		        $("#conteudo").html(dados);
    		    }
    	    });
    }
    remover = function(x)
    {
        var valor = $(x).attr('value');
        var url='/remover_post.php?id_post='+valor;
            $.ajax({
    		    type:'get', //Definimos o método HTTP usado   //Definimos o tipo de retorno
    		    url: url,   //Definindo o arquivo onde serão buscados os dados
    		    success: function(dados)
    		    {
                    alert(dados);
                    var tag="#post"+valor;
                    var tag_ok="#ok"+valor;
                    $(tag).empty();
                    $(tag_ok).empty();
    		    }
    	    });   
    }
    remover_comentario = function(x)
    {
        var valor = $(x).attr('value');
        var token=<?php echo"'".base64_encode($id)."'"; ?>;
        var url='/remover_comentario.php?id_comentario='+valor+"&token="+token;
            $.ajax({
    		    type:'get', //Definimos o método HTTP usado   //Definimos o tipo de retorno
    		    url: url,   //Definindo o arquivo onde serão buscados os dados
    		    success: function(dados)
    		    {
                    var tag="#comentario"+valor;
                    var tag_ok="#ok"+valor;
                    $(tag).empty();
                    $(tag_ok).empty();
    		    }
    	    });   
    }
</script>
</head>
<body id="content">
<!-- ===============================================--> 
<!--    Main Content--> 
<!-- ===============================================-->
<!--main class="main" id="top"-->
<div class="container">
<main class="container">
<div class="container">
    <?php include("config/barra_lateral.php");?>
    <div class="content">

    <div class="row no-gutters">
    <div class="col-lg-8 pr-lg-2">
        
    <div id="inicial" >
        <?php
        while($row=mysqli_fetch_array($res)){
    ?>
    <div <?php  echo"id='ok".$row['id_pubs']."'"; ?> >
    </div>
    <div class="card mb-3" <?php  echo"id='post".$row['id_pubs']."'"; ?> >
      <div class="card-header bg-light">
        <div class="row justify-content-between">
          <div class="col">
            <div class="media">
              <div class="avatar avatar-2xl"> 
              <img class="rounded-circle" <?php echo"src='".converter_imagem($row['img'])."'" ?> alt="" /> 
              </div>
              <div class="media-body align-self-center ml-2">
                <div class="row">
                <p class="mb-1 line-height-1">
                    
                        <div class="col-8">
                            <a class="font-weight-semi-bold" <?php echo"href='/perfil.php?usuario=".$row['usuario']."'";?> > 
                            <?php echo"".converter_usuario($row['usuario']); ?> </a>
                        </div>
                        <div class="col-4">
                            <?php echo"".opcoes_publicacao($row['id'],$row['id_pubs']); ?>
                        </div>
                    
                </p>
                </div>
                <p class="mb-0 fs--1"><?php echo"".converter_data($row['data']); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="card-body overflow-hidden">
        <p><?php 
        echo"".converter_texto($row['texto']); ?> </p>
                <?php 
                $link_imagens=converter_post_imagens($row['texto']);
                $link_video=converter_post_video($row['texto']);
                if($link_imagens[0]!=1){
                        if(sizeof($link_imagens)<2){?>
                <div class=" p-1"> 
                    <a <?php echo"href='".$link_imagens[0]."'" ?> data-fancybox="" data-caption="descrição de imagen">
                    <img class="rounded w-100" <?php echo"src='".$link_imagens[0]."'" ?> alt=""></a> 
                </div>
                <?php }else if(sizeof($link_imagens)>1){?>
                    <div class="row mx-n1">
                        <div class="col-6 p-1">
                            <a <?php echo"href='".$link_imagens[0]."'" ?> data-fancybox="gallery" data-caption="Image caption">
                            <img class="rounded w-100" <?php echo"src='".$link_imagens[0]."'" ?> alt=""></a>
                        </div>
                        <div class="col-6 p-1">
                            <a <?php echo"href='".$link_imagens[1]."'" ?> data-fancybox="gallery" data-caption="Image caption">
                            <img class="rounded w-100" <?php echo"src='".$link_imagens[1]."'" ?> alt="">
                            </a>
                        </div>
                    </div>
                <?php }else {
                            
                    
                            }
                
                }
                else if($link_imagens[0]==1)
                {
                    
                }
                if($link_video[0]!=1)
                {?>
                    <embed class="embed-responsive embed-responsive-16by9"  autoplay="autoplay"  <?php echo"src='".$link_video[0]."'";?>/>
                <?php
                    
                }
                else if($link_video[0]==1)
                {
                    
                }
                ?>
      </div>
      <div class="card-footer bg-light pt-0">
          <div class="row no-gutters font-weight-semi-bold text-center py-2 fs--1">
            <div class="col-auto">
                <button class="btn btn-light" onclick="curtir(this)" <?php echo"value='".$row['id_pubs']."'"; ?>>
                    <img src="/assets/imgs/like-activo.png" alt="" width="20">
                    <?php 
                        $sql_curtidas="SELECT * FROM tb_curtir_post where id_post=".$row['id_pubs'];
                        $res_curtidas=mysqli_query($linkdeconexao_db,$sql_curtidas);
                        $curtidas=mysqli_num_rows($res_curtidas);
                        ?>
                        <span class="ml-1"><?php echo"".$curtidas." "; ?>Curtir</span></button> 
            </div>
          </div>
          <?php 
            $sql_comentario_total="SELECT * FROM `tb_comentarios` WHERE `id_post=".$row['id_pubs'];
            $res_comentario_total=mysqli_query($linkdeconexao_db,$sql_comentario);
            $sql_comentario="SELECT u.img,u.id as id_user,u.usuario as nome_user,c.id,c.id_post,c.post,c.data_hora FROM `tb_comentarios` as c 
            inner join tb_user as u on u.id=c.usuario where c.id_post=".$row['id_pubs']." Limit 10";
            $res_comentario=mysqli_query($linkdeconexao_db,$sql_comentario);
            while($row_comentario=mysqli_fetch_array($res_comentario))
            {
                ?>
            <div class="card mb-3" <?php echo"id='comentario".$row_comentario['id']."'";?> >
                <div class="card-header bg-light">
                    <div class="media">
                    <div class="avatar avatar-2xl"> 
                        <img class="rounded-circle" <?php echo"src='".converter_imagem($row_comentario['img'])."'" ?> alt="" /> 
                    </div>
                    <div class="media-body align-self-center ml-2">
                        <div class="row">
                            <p class="mb-1 line-height-1">
                                <div class="col-8">
                                <a class="font-weight-semi-bold" <?php echo"href='/perfil.php?usuario=".$row_comentario['nome_user']."'";?> > 
                                <?php echo"".converter_usuario($row_comentario['nome_user']); ?> </a>
                            </div>
                            <div class="col-4">
                                <?php echo"".opcoes_comentario($row_comentario['id_user'],$row_comentario['id']); ?>
                            </div>
                            </p>
                        </div>    
                        <p class="mb-0 fs--1"><?php echo"".converter_data($row_comentario['data_hora']); ?></p>
                    </div>
                </div>
                </div>
                <div class="card-body overflow-hidden">
                    <p> <?php echo"".$row_comentario['post'];?> </p>
                </div>    
            </div>
                  <?php 
                  if(mysqli_num_rows($res_comentario_total)>10){?>
                        <div class="card mb-3">
                            <button onclick="comentario(this)" type="button" class="btn btn-light" onClick="Mais_comentario">Mostrar Mais Comentarios</button>
                        </div>
            <?php }
            }
          ?>
          <div id='comentario_ok'></div>
          <div class="card mb-3">
                <div class="card-header bg-light">
                    <div class="media">
                    <div class="avatar avatar-2xl"> 
                        <img class="rounded-circle" <?php echo"src='".converter_imagem($img)."'" ?> alt=""> </div>
                    <div class="media-body align-self-center ml-2">
                        <p class="mb-1 line-height-1"><a class="font-weight-semi-bold" href="#"><?php echo"".converter_usuario($row_comentario['usuario'])."" ?></a></p>
                    </div>
                    </div>
                </div>
                <div class="card-body overflow-hidden">
                    <div id="progressDivCo" style="display:none">
                        <img src="/file/img_padrao/loading.gif" width="80" heigth="80">
                    </div>
                      <form id="CForm" method="Post">
                            <div class="col-12">
                                <input name="token" type="hidden" <?php echo"value='".base64_encode($id)."'"; ?> >
                                <input name="pubs" type="hidden" <?php echo"value='".$row['id_pubs']."'"; ?> >
                                <input name="texto" class="form-control rounded-capsule ml-2 fs--1" type="text" placeholder="Escreva um Comentário...">
                            </div>
                            <br>
                            <div class="float-sm-left">
                                <button  type="submit" class="btn btn-light" id="submitComentario">
                                    <img src="/assets/imgs/comment-activo.png" alt="" width="20"><span class="ml-1">Comentar</span>
                                </button>
                            </div>
                      </form>   
                </div>
        </div>
    </div>
    </div>
    <?php }?>
    <input type="hidden" id="row" value="0">
    <input type="hidden" id="all" <?php echo"value='".$total."'"?>>
    
  </div>
  <div id="conteudo">
  </div>
</div>
    <?php include("config/barra_ranking.php"); ?>
</div>
</div>
</div>
</main>
</div>

<!--/main-->

<!-- ===============================================--> 
<!--    End of Main Content--> 
<!-- ===============================================--> 

<!-- Principal JavaScript do Bootstrap
    ================================================== --> 
<!-- Foi colocado no final para a página carregar mais rápido --> 

<script src="/assets/js/vendor/popper.min.js"></script> 
<script src="/dist/js/bootstrap.min.js"></script>
<script src="/fancybox/jquery.fancybox.min.js"></script> 
<script src="/select2/select2.min.js"></script> 
<script src="/@fortawesome/all.min.js"></script>
</body>
</html>
