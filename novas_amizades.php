<?php
session_start();
include("config/header.php");
if(isset($_GET['usuario']))
{

header('Content-Type: text/html; charset=utf-8');
$buscar=$_GET['usuario'];    
$sql="SELECT u.id as id_usuario, u.id_parceiro,p.id,u.idade as aniversario ,p.nome as nome_parceiro,p.genero as genero_parceiro,
p.Idade as aniversario_parceiro,u.nome as nome,u.img as img, u.imgcapa as imgcapa, u.genero as genero,
u.usuario as usuario,u.usuario_apresentacao as usuario_apresentacao,u.mora_cidade as mora_cidade,
u.mora_estado as mora_estado ,u.cor_cabelo as cor_cabelo FROM `tb_user` as u 
left join tb_parceira as p on u.id_parceiro=p.id
where u.usuario = '$buscar' ";
$res=mysqli_query($linkdeconexao_db,$sql);
$row=mysqli_fetch_array($res);

$sql_interesse="SELECT * FROM tb_interesse as i
inner join tb_genero as g on g.id=i.id_genero
WHERE id_user = ".$row['id_usuario'];
$res_interesse=mysqli_query($linkdeconexao_db,$sql_interesse);

$sql_amigos="SELECT u.img as imagem_de, u.usuario as usuario_de,u.id as id_de,u1.id as id_para ,u1.img as imagem_para,u1.usuario as usuario_para FROM `tb_amizades` as a 
INNER JOIN tb_user as u on u.id=a.de 
INNER JOIN tb_user as u1 on u1.id=a.para
where (a.de = ".$row['id_usuario']." ) or (a.para = ".$row['id_usuario'].")";
$res_amigos=mysqli_query($linkdeconexao_db,$sql_amigos);

if($row['usuario']==$nome_usuario)
{
    header("Location: /editar_perfil.php");	
}
?>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>SexWS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1">
<meta name="description" content="" />
<meta name="keywords" content="" />

    <!-- Principal CSS do Bootstrap -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Estilos customizados -->
	<link href="dist/css/dashboard.css" rel="stylesheet">
	<link href="dist/css/theme.css" rel="stylesheet">
	<link href="dist/css/jquery.privews.min.css" rel="stylesheet">
	<link href="fancybox/jquery.fancybox.min.css" rel="stylesheet">
	<link href="select2/select2.min.css" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.form.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $('#btn_amizade').click(function() 
        {
            var valor=<?php echo"'".$row['usuario']."'";?>;
            var url='/ser_amigo.php?nome_usuario='+valor;
            $.ajax({
    		    type:'post', //Definimos o método HTTP usado   //Definimos o tipo de retorno
    		    url: url,   //Definindo o arquivo onde serão buscados os dados
    		    success: function(dados)
    		    {
    		        $('#btn_amizade').empty();
    		        $('#btn_amizade').append(dados);
    		    }
    	    });
        });
    });
</script>
</head>
<body>
    <main class="container">
		<!-- imagem cabeçalho-->
		<div>
  		<img <?php echo"src='".converter_imagem_capa($row['imgcapa'])."'" ?> class="img-fluid" alt="perfil">
			<div class="row perfil">
			<div class="col-md-3">
				<img <?php echo"src='".converter_imagem($row['img'])."'" ?> width="150" height="150" alt="perfil" class=" mx-auto d-block rounded-circle mx-auto avatar avatar-5xl"/> 
			</div>
			<div class="col-md-6">
				<span class="align-baseline"><h3><?php echo"".$row['nome']; ?></h3></span>
				</div>
      		</div>
		</div>
		<!-- dados perfil lateral-->
		<div class="container-fluid">
            <div class="row">
                <div>
                    <ul class="nav flex-column">
                      <li class="nav-item">
                        <a class="nav-link text-center text-danger" href="#">
                          <span data-feather="apresentacao"></span>
                          Apresentação
                        </a>
        				  <p class="text-center"><?php echo"".$row['usuario_apresentacao']."" ?></p>
                      </li>     
                      <li class="nav-item">
                        <a class="nav-link text-danger" href="#">
                          <span class="fas fa-user "></span>
                          <?php 
                            echo"".casal_solteiro($row['id_parceiro']);
                          ?>
                        </a>
                      </li>   
                      <li class="nav-item">
                        <a class="nav-link text-danger" href="#">
                          <span class="fas fa-map-marker-alt "></span>
                          <?php 
                            echo"".$row['mora_estado']."-".$row['mora_cidade'];
                          ?>
                        </a>
                      </li>   
        			  <li class="nav-item">
                        <a class="nav-link text-danger" href="#">
                          <span class="fas fa-hand-point-right "></span>
                          <?php 
                            echo"Seguindo por ".$row['seguidores']." Pessoas";
                          ?>
                        </a>
                      </li> 
                      <?php if(mysqli_num_rows($res_interesse)>0){?>
        			  <li class="nav-item">
                          <span class="fas fa-thumbs-up"></span>
                            Interessado em 
                          <br> 
                          <?php
                                  while($row_interesse=mysqli_fetch_array($res_interesse))
                                  {
                                        echo"<a class='nav-link text-danger' href='/buscar.php?pesquisa=".$row_interesse['Apelido']."'>".$row_interesse['Apelido']."</a>";
                                  }
                          ?>
                      </li> 
                      <?php }?>
        			  <li class="nav-item">
        			      <?php 
        		                $sql_amizades="SELECT * FROM `tb_amizades` where (de = $id and para = ".$row['id_usuario'].") or (de = $id and para = ".$row['id_usuario'].")";
        		                $res_amizades=mysqli_query($linkdeconexao_db,$sql_amizades);
        		                $row_amizades=mysqli_fetch_array($res_amizades);
        		                if(mysqli_num_rows($res_amizades)>0){
        		                    if($row_amizades['aceite']=="pen")
        		                    {
        			      ?>
        			                <button id='btn_amizade' type="button" class="btn btn-danger btn-lg btn-block">Pedido De Amizade Pedente</button>
                                    <?php }
                                    else {?>
                          <button id='btn_amizade' type="button" class="btn btn-danger btn-lg btn-block">Desfazer Amizade</button>
                          <?php } 
                                }else { ?>
                          <button id='btn_amizade' type="button" class="btn btn-danger btn-lg btn-block">Ser Amigo</button>
                          <?php }?>
                      </li>
                    </ul>  
                  </div>
    
    		  <!----- conteúdo ------>
    
                <main role="main" class="col-md-8 ml-sm-auto col-lg-9 px-2">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
		        <div class="row">
		        <div class="saf col align-self-end">
		        <nav class="navbar mb-2">
			   <ul class="nav">
                    <li class="nav-link disabled">
                    <?php echo"".converter_genero($row['genero'])." tem ".converter_idade($row['aniversario']);
                    if(isset($row['genero_parceiro']))
                    {
                        echo"-".converter_genero($row['genero_parceiro'])." tem ".converter_idade($row['aniversario_parceiro']); 
                    }
                    ?>
                    </li>
                    <li class="nav-item"><a class="nav-link" <?php echo"href='/novas_amizades.php?amigos=".$row['usuario']."'"?> >Amigos</a></li>
    			        <li class="nav-item"><a class="nav-link" <?php echo"href='/album.php?usuario=".$row['usuario']."'"?>>Foto Log</a></li>
    		        </ul>
    		</nav>
    		</div>
    		</div> 
            </div>
              <div class="rounded f-publicar">
    			<center><h4>Amigos</h4></center>
    			    <div class="container">
    			     <?php if(mysqli_num_rows($res_amigos)>0){
                            while($row_amigos=mysqli_fetch_array($res_amigos))
                            {
                                if($row_amigos['id_para'] == $row['id_usuario'])
                                {
                                    $usuario=$row_amigos['usuario_de'];
                                    $imagem=$row_amigos['imagem_de'];
                                }
                                else if($row_amigos['id_de'] == $row['id_usuario'])
                                {
                                    $usuario=$row_amigos['usuario_para'];
                                    $imagem=$row_amigos['imagem_para'];
                                }
                            ?>    
                                <div class="col-1">
                                </div>
                                <div class="col-4">
                                    <div class="bg-white pt-4 h-300">
                                        <center>
                                            <div class="avatar avatar-4xl"> 
                                            <img class="rounded-circle" <?php echo"src='".converter_imagem($imagem)."'" ?> alt="" /> 
                                            </div>
                                            <div class="media-body align-self-center ml-2">
                                                <p class="mb-1 line-height-1"><a class="font-weight-semi-bold" <?php echo"".converter_link($usuario)."" ?>>
                                                   <?php echo"".converter_usuario($usuario)."" ?></a></p>
                                            </div>
                                            </div>
                                        </center>
                                </div>
                                <div class="col-1">
                                </div>
                    <?php      }
                    }else{
                        echo"<br><center><h6>Não Tem Amigos.</h6></center>";
                    }?>    
            
    			</div>
                </div>
        </main>
            </div>    
        </div>  
    </main> 
            </div>
        </div>
    <!-- Principal JavaScript do Bootstrap
    ================================================== -->
    <!-- Foi colocado no final para a página carregar mais rápido -->
    <!--script src="file:///C|/code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script-->
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
	
    <script src="assets/js/vendor/popper.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="dist/js/theme.js"></script>
	<script src="dist/js/jquery.min.js"></script>  
	<script src="fancybox/jquery.fancybox.min.js"></script>
	<script src="select2/select2.min.js"></script>
	<script src="@fortawesome/all.min.js"></script>
    
</body>
</html>
<?php
}else{
$var = "<script>javascript:history.back(-2)</script>";
echo $var;
}?>  