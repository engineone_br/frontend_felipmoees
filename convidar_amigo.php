<?php
include("config/header.php");
if (isset($_POST['enviar-convite'])) 
{
	$email = $_POST['email-convidado'];
	$conheceRealVirtual = $_POST['conhece-real-ou-virtual'];
	$soloOuCasal = $_POST['solo-ou-casal'];
	$generoPessoa1 = $_POST['genero-pessoa1'];
	$generoPessoa2 = $_POST['genero-pessoa2'];
    $data = date("y/m/d");
    $codConvite = md5(rand());
    $email_check = mysqli_query($linkdeconexao_db, "SELECT email FROM tb_user WHERE email='$email'");
    $do_email_check = mysqli_num_rows($email_check);
    if ($do_email_check >= 1) {
        echo '<h3>Este email já está registado</h3>';
    }else{
        $query = "INSERT INTO tb_convites (`usuario_de`,`usuario_para`,`real_ou_virtual`,`solo_ou_casal`,`cod_convite`,`genero_pessoa1`,`genero_pessoa2`) VALUES ('$login_cookie','$email','$conheceRealVirtual','$soloOuCasal','$codConvite','$generoPessoa1','$generoPessoa2')";
        $data = mysqli_query($linkdeconexao_db, $query);
        if ($data) 
        {
            ini_set('display_errors', 1);
		    error_reporting(E_ALL);
		    $from = "convitesw@privesw.com.br";
		    $to = $email;
		    $message = "<html>
            <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
                
                <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
                
                <!-- Bootstrap CSS -->
                
                <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css' integrity='sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4' crossorigin='anonymous'>
                <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.0.10/css/all.css' integrity='sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg' crossorigin='anonymous'>
                <link href='https://fonts.googleapis.com/css?family=Gugi|Roboto:400,700' rel='stylesheet'>
                
            </head>
            <body>
            <div class='row'>
            <form action = 'https://privesw.com.br/registro.php' method='get'>
            <p class='center login-form-text'>Seja Bem Vindo Ao privesw.</p><br>
            <input type='hidden'  value='$alphaNumeric' name='code'> 
            <button class='btn waves-effect waves-light col s12 m12 m12' type='submit' >Click Aqui Para Ativar A Sua Conta </button>
            </form>
            </div>
            </body>
            </html>";
                $headers="Content-type: text/html; charset=iso-8859-1\r\n";
                $headers.="From:".$from;
                $enviado=mail($email, $subject, $message, $headers);
            
		echo "A mensagem de e-mail foi enviada.";
        }else{
            echo mysqli_error($linkdeconexao_db);
//            echo "<h3>Desculpe, houve um erro e não foi possivel enviar o convite</h3>";
        }
    }

}

//action="send-mail.php"
?>
<!DOCTYPE html>
<html>
<head>
	<title>Convidar amigo</title>
	<style type="text/css">
		div#convidar-amigo{margin-top: 200px;}
		input{width: 600px;}
		div#menuMain{display: none;}
	</style>
</head>
<body>
	<div id="convidar-amigo">
		<form method="POST" >
			<input type="email" placeholder="Endereço de email do seu amigo(a)" name="email-convidado"><br/><br/>


			<span class="conhece-real-ou-virtual">Você o(a) conhece na vida real ou apenas no mundo virtual?</span>
			<select name="conhece-real-ou-virtual">
				<option value="Amigo real">Amigo real</option>
				<option value="Amigo virtual">Amigo virtual</option>
			</select>
			<br/><br/>

			<span class="solo-ou-casal">Seu amigo(a) é um casal ou é solteiro(a)?</span>
			<select name="solo-ou-casal">
				<option value="Solteiro">Solteiro</option>
				<option value="Casal">Casal</option>
			</select><br/><br/>

			<span class="genero-pessoa1">Qual o genero do(a) seu amigo(a)?</span>
			<select name="genero-pessoa1">
                <option value="Ela">Ela</option>
                <option value="Ele">Ele</option>
			</select><br/><br/>
			<span class="genero-pessoa2">Qual genero do(a) parceiro(a) do(a) seu amigo(a)?</span>
			<select name="genero-pessoa2">
                <option value=""></option>
                <option value="Ela">Ela</option>
                <option value="Ele">Ele</option>
			</select><br/><br/>


<!--            <span class="tipo-casal">Qual o tipo de casal?</span>-->
<!--            <select name="tipo-casal">-->
<!--                <option value="Ela">Ela</option>-->
<!--                <option value="Ele">Ele</option>-->
<!--            </select><br/><br/>-->

			<input type="submit" name="enviar-convite" value="Enviar convite">
		</form>
	</div>
</body>
</html>