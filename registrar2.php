<?php
session_start();
include("config/db.php");
if(isset($_GET['code']))
{
    $cod=$_GET['code'];
    $verifica = mysqli_query($linkdeconexao_db, "SELECT * FROM tb_convites where cod_convite='$cod'");
    if(mysqli_num_rows($verifica)==0)
    {
		echo "<div class='alert alert-danger' role='alert'>Convite invalido</div>";
        header("Location: /index.php");    
    }
    else
    {
        while($row=mysqli_fetch_array($verifica))
        {
            $email=$row['usuario_para_email'];
            $genero1=$row['genero_pessoa1'];
            $id=$row['id_convites'];
            if($row['solo_ou_casal']=="Casal")
            {
                $genero2=$row['genero_pessoa2'];
            }
        }
    }
}
else
{
    header("Location: /index.php");
}
?>

<!doctype html>
<html lang="pt-br">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Gugi|Roboto:400,700" rel="stylesheet">

    <title>Privê SW | Rede Social Adulta</title>
    <style>
        html,
        body {
            height: 100%;
        }

        body {
            font-family: 'Roboto', sans-serif;
            height: 100%;

        }

        .agendo {
            font-family: 'Gugi', cursive;
        }

        .f16 {
            font-size: 16px;
        }

        .linha0 {
            line-height: 0.9;
        }

        .container-fluid {
            height: 100%;
        }

        .fundo_login {
            background-image: url('/assets/imgs/fundo.png');
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>

<body>
    
<?php
	if(isset($_POST['nome']) && isset($_POST['data_nascimento1'])) {
		$nome = $_POST['nome'];
		$data1 =$_POST['data_nascimento1'];
		$cabelo1 =$_POST['cabelo_pessoa1'];
		if(strlen($_POST['nome'])<4)
		{
    		echo "<div class='alert alert-danger' role='alert'>Nome Invalido</div>";
		}
		else if(strlen($_POST['data_nascimento1'])<4)
		{
    		echo "<div class='alert alert-danger' role='alert'>Data De Nascimento Invalido</div>";
		}
		else if(strlen($_POST['cabelo_pessoa1'])<3)
		{
    		echo "<div class='alert alert-danger' role='alert'>Cor Do Cabelo Invalido</div>";
    		echo"cabelo: ".$_POST['cabelo_pessoa1'];
		}
		else if(strlen($_POST['cabelo_pessoa1'])>3 && strlen($_POST['data_nascimento1'])>4  && strlen($_POST['nome'])>4)
		{
            		$sql="INSERT INTO tb_parceira(nome,idade,cor_cabelo,genero)VALUES('$nome','$data1','$cabelo1',$genero2)";
            		echo"comando:".$sql;
            		if(mysqli_query($linkdeconexao_db,$sql))
            		{
            		   $parceira=mysqli_insert_id($linkdeconexao_db);
            		   $sql="UPDATE tb_user SET id_parceiro=$parceira WHERE email='$email'";
            		    if(mysqli_query($linkdeconexao_db,$sql))
                    	{
            		            echo "<div class='alert alert-success' role='alert'>Salvo Com Sucesso.</div>";
            		            $_SESSION['Salvo_Sucesso']="ok";
            		     header("Location: /index.php");       
                    	}
            		    
            		}
		}
	}
?>
    
    <div class="container-fluid">

        <div class="row" style="height: 100%;">
            <div class="col-md-6 col-sm-12 col-xs-12 d-none d-sm-none d-md-block d-xs-none fundo_login">
                <center><img style=" margin-top: 30%;" src="/assets/imgs/logo_fundo_dark.png" height="130"></center>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12 pl-5 pr-5 pb-3" style="background-image: linear-gradient(to bottom, purple, black); color:#fff;">
                <!-- Login -->

                <div class="col-md-8 offset-md-2 col-sm-12 offset-sm-0 col-xs-12 offset-xs-0 mt-5">
                    <br>
                    <p class="h5 linha0 mt-5"><span class="font-weight-bold">Seja Muito Bem-Vindo(a) à Privê SW</span><br><small class="f16">Efetue O Cadastro Do(a) Seu Parceiro(a)</small></p>

                    <form <?php echo"action='registrar2.php?code=".$_GET['code']."'" ?> method="post" accept-charset="utf-8" class="mt-5">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Nome</label>
                            <input type="text" name="nome" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="O Seu Nome " <?php echo"value='".$nome."'" ?> >
                        </div>
                          <div class="form-group">
                            <label for="exampleFormControlInput1">Data De Nascimento Da Sua Parceiro(a)</label>
                            <input type="date" class="form-control" name="data_nascimento1" <?php echo"value='".$data1."'" ?>>
                          </div>
                          <div class="form-group">
                              <label for="exampleFormControlInput1">Cor Do Cabelo Do(a) Sua Parceiro(a)</label>
                            <input type="text" class="form-control" name="cabelo_pessoa1" placeholder="Sua cor de cabelo"<?php echo"value='".$cabelo."'" ?>>
                          </div>
                        <button type="submit" class="btn btn-danger btn-sm">Cadastrar</button>
                    </form>
                </div>

            </div>


        </div>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>

</html>