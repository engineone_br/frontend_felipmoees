<?php
session_start();
include("config/header.php");
if(isset($_GET['capa']))
{
    if (isset($_FILES['arquivo']['name']) && $_FILES['arquivo']['error'] == 0 )
    {
        $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
        $nome = $_FILES['arquivo']['name'];
        // Pega a extensão
        $extensao = pathinfo ( $nome, PATHINFO_EXTENSION );
        // Converte a extensão para minúsculo
        $extensao = strtolower ( $extensao );
        // Somente imagens, .jpg;.jpeg;.gif;.png
        // Aqui eu enfileiro as extensões permitidas e separo por ';'
        // Isso serve apenas para eu poder pesquisar dentro desta String
        if (strstr('.jpg;.jpeg;.gif;.png',$extensao)) 
        {
				// Cria um nome único para esta imagem
				// Evita que duplique as imagens no servidor.
				$novoNome = time();
				// Concatena a pasta com o nome
				$destino ="file/img_capa/".$novoNome; 
				$destino1 ="file/img_capa/".$novoNome.".png";
             // tenta mover o arquivo para o destino
				// tenta mover o arquivo para o destino
					imagepng(imagecreatefromstring(file_get_contents($arquivo_tmp)),$destino1);
					$width = 1500;
					$height = 800;
				// O resize propriamente dito. Na verdade, estamos gerando uma nova imagem.
				$image_p = imagecreatetruecolor($width, $height);
				$image = imagecreatefrompng($destino1);		
					// Obtendo o tamanho original
					$width_orig = imagesx($image);
					$height_orig = imagesy($image);
					imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
					// Ou, se preferir, Salvando a imagem em arquivo:
					imagepng($image_p,$destino1);
			if(move_uploaded_file ( $arquivo_tmp, $destino ) ) 
			{
				unlink($destino);
				if($capa!="Padrao")
				{
				    unlink($img);
				}
                $sql="UPDATE `tb_user` SET imgcapa='$destino1' WHERE email='$email'";
			    if(mysqli_query($linkdeconexao_db,$sql))
			    {
			        echo "<div class='alert alert-success' role='alert'>Salvo Com Sucesso.</div>";
			        unset($_POST);
			        unset($_FILE);
			    }
                exit();
			}
            else
            {
		        echo "<div class='alert alert-danger' role='alert'>Erro ao salvar o arquivo. Aparentemente você não tem permissão de escrita</div>";
            }
        }
        else{
		        echo "<div class='alert alert-danger' role='alert'>Você poderá enviar apenas arquivos *.jpg;*.jpeg;*.gif;*.png</div>";
            }
            
    }   
}
else if(isset($_GET['perfil']))
{
    if (isset($_FILES['arquivo']['name']) && $_FILES['arquivo']['error'] == 0 )
    {
        $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
        $nome = $_FILES['arquivo']['name'];
        // Pega a extensão
        $extensao = pathinfo ( $nome, PATHINFO_EXTENSION );
        // Converte a extensão para minúsculo
        $extensao = strtolower ( $extensao );
        // Somente imagens, .jpg;.jpeg;.gif;.png
        // Aqui eu enfileiro as extensões permitidas e separo por ';'
        // Isso serve apenas para eu poder pesquisar dentro desta String
        if (strstr('.jpg;.jpeg;.gif;.png',$extensao)) 
        {
				// Cria um nome único para esta imagem
				// Evita que duplique as imagens no servidor.
				$novoNome = time();
				// Concatena a pasta com o nome
				$destino ="file/img_perfil/".$novoNome; 
				$destino1 ="file/img_perfil/".$novoNome.".png";
             // tenta mover o arquivo para o destino
				// tenta mover o arquivo para o destino
					imagepng(imagecreatefromstring(file_get_contents($arquivo_tmp)),$destino1);
					$width = 400;
					$height = 400;
				// O resize propriamente dito. Na verdade, estamos gerando uma nova imagem.
				$image_p = imagecreatetruecolor($width, $height);
				$image = imagecreatefrompng($destino1);		
					// Obtendo o tamanho original
					$width_orig = imagesx($image);
					$height_orig = imagesy($image);
					imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
					// Ou, se preferir, Salvando a imagem em arquivo:
					imagepng($image_p,$destino1);
			if(move_uploaded_file ( $arquivo_tmp, $destino ) ) 
			{
				unlink($destino);
				if($img!="Padrao")
				{
				    unlink($img);
				}
                $sql="UPDATE `tb_user` SET img='$destino1' WHERE email='$email'";
			    if(mysqli_query($linkdeconexao_db,$sql))
			    {
			        echo "<div class='alert alert-success' role='alert'>Salvo Com Sucesso.</div>";
			        unset($_POST);
			        unset($_FILE);
			    }
                exit();
			}
            else
            {
		        echo "<div class='alert alert-danger' role='alert'>Erro ao salvar o arquivo. Aparentemente você não tem permissão de escrita</div>";
            }
        }
        else{
		        echo "<div class='alert alert-danger' role='alert'>Você poderá enviar apenas arquivos *.jpg;*.jpeg;*.gif;*.png</div>";
            }
            
    }
}

?>