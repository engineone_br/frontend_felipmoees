<?php
if(isset($_POST['texto']))
{
    if(strlen($_POST['texto'])>4)
    {
        date_default_timezone_set('America/Sao_Paulo');
        $data = date('Y-m-d H:i:s');
        if (isset($_FILES['img']['name']))
        {
            $link=array();
            $tamanho=$_POST['tamanho'];
            echo"Tamanho = ".$tamanho."<br>";
            for($i = 0; $i < $tamanho; $i++)
            {    
                $arquivo_tmp = $_FILES['img']['tmp_name'][$i];
            $nome = $_FILES['img']['name'][$i];
            // Pega a extensão
            $extensao = pathinfo ( $nome, PATHINFO_EXTENSION );
            // Converte a extensão para minúsculo
            $extensao = strtolower ( $extensao );
            // Somente imagens, .jpg;.jpeg;.gif;.png
            // Aqui eu enfileiro as extensões permitidas e separo por ';'
            // Isso serve apenas para eu poder pesquisar dentro desta String
            if (strstr('.jpg;.jpeg;.gif;.png',$extensao)) 
            {
    				// Cria um nome único para esta imagem
    				// Evita que duplique as imagens no servidor.
    				$novoNome = time();
    				// Concatena a pasta com o nome
    				$destino ="file/img_post".$novoNome; 
    				$destino1 ="file/img_post".$novoNome.".png";
            		// tenta mover o arquivo para o destino
            		imagepng(imagecreatefromstring(file_get_contents($arquivo_tmp)),$destino1);
            		$width = 400;
            		$height = 400;		
                   // O resize propriamente dito. Na verdade, estamos gerando uma nova imagem.
                   $imagem = imagecreatetruecolor($width, $height);
                   $image = imagecreatefrompng($destino1);
            		// Obtendo o tamanho original
            		$width_orig = imagesx($image);
            		$height_orig = imagesy($image);
                   imagecopyresampled($imagem, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
                   // Ou, se preferir, Salvando a imagem em arquivo:
                   imagepng($imagem,$destino1);
                   
		            $imagemLogo = imagecreatefrompng("logo.png");
                    
                    // Copia a logo para a imagem
                    imagecopymerge( $imagem, $imagemLogo,0,350,0,0,400,50,8);
                    imagepng($imagem,$destino1);
    			if(move_uploaded_file ( $arquivo_tmp, $destino ) ) 
    			{
    				unlink($destino);   
    				array_push($link,$destino1);
    			    echo "<img src='$destino1'>";
    			}
                else
                {
                    echo"error";
    		        //echo "<div class='alert alert-danger' role='alert'>Erro ao salvar o arquivo. Aparentemente você não tem permissão de escrita</div>";
    		        exit();
                }
            }
            else
            {
                echo"erro";
    		        //echo "<div class='alert alert-danger' role='alert'>Você poderá enviar apenas arquivos *.jpg;*.jpeg;*.gif;*.png</div>";
            }
            }
                    $texto=array('Imagens'=>array(0=>$link[0],1=>$link[1]),'Videos'=>null,'Texto'=>$_POST['texto']);
                        $texto=json_encode($texto);
                        $sql="INSERT INTO tb_pubs(usuario,texto,data)VALUES($id,'$texto','$data')";
                        echo"<br>comando:".$sql;
                        if(mysqli_query($linkdeconexao_db,$sql))
                        {
                            unset($_POST['texto']);
                              echo "<div class='alert alert-success' role='alert'>Publicado Com Sucesso.</div>";
                        }
                        else
                        {
                            echo"<div class='alert alert-danger' role='alert'>Erro Ao Publicar </div>";
                        }    
        }
        else
        {   
            $texto=array('Imagens'=>null,'Videos'=>null,'Texto'=>$_POST['texto']);
            $texto=json_encode($texto);
            $sql="INSERT INTO tb_pubs(usuario,texto,data)VALUES($id,'$texto','$data')";
            if(mysqli_query($linkdeconexao_db,$sql))
            {
                  echo "<div class='alert alert-success' role='alert'>Publicado Com Sucesso.</div>";
            }
            else
            {
                echo"<div class='alert alert-danger' role='alert'>Erro Ao Publicar </div>";
            }
        }
    }
    else
    {
        echo"<div class='alert alert-danger' role='alert'>Texto Muito Curto</div>";
    }
}
?>