<?php
session_start();
include("config/db.php");
if(isset($_GET['code']))
{
    $cod=$_GET['code'];
    $verifica = mysqli_query($linkdeconexao_db, "SELECT * FROM tb_convites where cod_convite='$cod'");
    if(mysqli_num_rows($verifica)==0)
    {
		echo "<div class='alert alert-danger' role='alert'>Convite invalido</div>";
        header("Location: /index.php");    
    }
    else
    {   
            while($row=mysqli_fetch_array($verifica))
            {
                if($row['usuario_para']==null)
                {
                    $email=$row['usuario_para_email'];
                    $genero1=$row['genero_pessoa1'];
                    $id=$row['id_convites'];
                    if($row['solo_ou_casal']=="Casal")
                    {
                        $genero2=$row['genero_pessoa2'];
                    }
                }
                else
                {
            		echo "<div class='alert alert-danger' role='alert'>Convite invalido</div>";
                    header("Location: /index.php"); 
                }
            }
        
    }
}
else
{
    header("Location: /index.php");
}
?>

<!doctype html>
<html lang="pt-br">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Gugi|Roboto:400,700" rel="stylesheet">

    <title>Privê SW | Rede Social Adulta</title>
    <style>
        html,
        body {
            height: 100%;
        }

        body {
            font-family: 'Roboto', sans-serif;
            height: 100%;

        }

        .agendo {
            font-family: 'Gugi', cursive;
        }

        .f16 {
            font-size: 16px;
        }

        .linha0 {
            line-height: 0.9;
        }

        .container-fluid {
            height: 100%;
        }

        .fundo_login {
            background-image: url('/assets/imgs/fundo.png');
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.form.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () 
    {
        $.ajax({
    		    type:'get', //Definimos o método HTTP usado   //Definimos o tipo de retorno
    		    url: 'https://servicodados.ibge.gov.br/api/v1/localidades/estados/',   //Definindo o arquivo onde serão buscados os dados
    		    success: function(json)
    		    {
                    var options = '<option value="">–  –</option>';
                    for (var i = 0; i < json.length; i++) 
                    {
                        options += '<option data-id="' + json[i].id + '" value="' + json[i].nome + '" >' + json[i].nome + '</option>';
                    }
                       $("#sd").html(options);
    		    }
       
    });
        $("#sd").change(function () {
 
        if ($(this).val()) {
            $.getJSON('https://servicodados.ibge.gov.br/api/v1/localidades/estados/'+$(this).find("option:selected").attr('data-id')+'/municipios', {id: $(this).find("option:selected").attr('data-id')}, function (json) {
 
                var options = '<option value="">–  –</option>';
 
                for (var i = 0; i < json.length; i++) {
 
                    options += '<option value="' + json[i].nome + '" >' + json[i].nome + '</option>';
 
                }
 
                $("select[name='cidade']").html(options);
 
            });
 
        } else {
 
            $("select[name='cidade']").html('<option value="">–  –</option>');
 
        }
 
    });
    });
    </script>
</head>

<body>
    
<?php
	if(isset($_POST['nome_usuario']) && isset($_POST['senha'])) {
		$senha = $_POST['senha'];
		$senha_novamente = $_POST['senha_novamente'];
		$nome = $_POST['nome'];
		$sobrenome = $_POST['sobrenome'];
		$nome_usuario = $_POST['nome_usuario'];
		$data = date("y-m-d");
		$data1 =$_POST['data_nascimento1'];
		$cabelo1 =$_POST['cabelo_pessoa1'];
		
		$estado =$_POST['estado'];
		$cidade =$_POST['cidade'];
		if(strlen($_POST['nome'])<4)
		{
    		echo "<div class='alert alert-danger' role='alert'>Nome Invalido</div>";
		}
		if(strlen($_POST['sobrenome'])<4)
		{
    		echo "<div class='alert alert-danger' role='alert'>Sobrenome Invalido</div>";
		}
		if(strlen($_POST['nome_usuario'])<4)
		{
    		echo "<div class='alert alert-danger' role='alert'>Nome De Usuario Invalido</div>";
		}
		if(strlen($_POST['data_nascimento1'])<4)
		{
    		echo "<div class='alert alert-danger' role='alert'>Data De Nascimento Invalido</div>";
		}
		if(strlen($_POST['cabelo_pessoa1'])<3)
		{
    		echo "<div class='alert alert-danger' role='alert'>Cor Do Cabelo Invalido</div>";
    	}
		if(strlen($_POST['senha'])<4)
		{
    		echo "<div class='alert alert-danger' role='alert'>Senha Muito Curta</div>";
		}
		if(strlen($_POST['cabelo_pessoa1'])>3 && strlen($_POST['data_nascimento1'])>4 && strlen($_POST['nome_usuario'])>4 && strlen($_POST['sobrenome'])>4 && strlen($_POST['nome'])>4 && strlen($_POST['senha'])>4)
		{
    		if($_POST['senha']==$_POST['senha_novamente'])
    		{
    		    $verifica = mysqli_query($linkdeconexao_db, "SELECT * FROM tb_user where usuario='$nome_usuario'");
                if(mysqli_num_rows($verifica)==0)
                {
            		$sql="INSERT INTO tb_user(email,senha,data_inscricao,nome,sobrenome,idade,cor_cabelo,genero,id_convites,mora_cidade,mora_estado,usuario)VALUES
            		('$email','$senha','$data','$nome','$sobrenome','$data1','$cabelo1',$genero1,$id,'$cidade','$estado','$nome_usuario')";
            		if(mysqli_query($linkdeconexao_db,$sql))
            		{  
            		    $convite=mysqli_insert_id($linkdeconexao_db);
            		   $sql="UPDATE `tb_convites` SET usuario_para=$convite WHERE usuario_para_email='$email'";
            		   if(mysqli_query($linkdeconexao_db,$sql))
                	   {
                		    echo "<div class='alert alert-success' role='alert'>Salvo Com Sucesso.</div>";
                		    if(isset($genero2))
                		    {
                		        header("Location: /registrar2.php");
                		    }
                		    header("Location: /");
            		    }
            		}
            		
                }
                else
                {
            		echo "<div class='alert alert-danger' role='alert'>Nome De Usuario Já Cadastrado.</div>";
                }
    		}
        	else
        	{
        		echo "<div class='alert alert-danger' role='alert'>Senha não conferem!</div>";
        	}
		}
	}
?>
    
    <div class="container-fluid">

        <div class="row" style="height: 100%;">
            <div class="col-md-6 col-sm-12 col-xs-12 d-none d-sm-none d-md-block d-xs-none fundo_login">
                <center><img style=" margin-top: 30%;" src="/assets/imgs/logo_fundo_dark.png" height="130"></center>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12 pl-5 pr-5 pb-3" style="background-image: linear-gradient(to bottom, purple, black); color:#fff;">
                <!-- Login -->

                <div class="col-md-8 offset-md-2 col-sm-12 offset-sm-0 col-xs-12 offset-xs-0 mt-5">
                    <br>
                    <p class="h5 linha0 mt-5"><span class="font-weight-bold">Seja Muito Bem-Vindo(a) à Privê SW</span><br><small class="f16">Efetue seu Login para Acessar</small></p>

                    <form <?php echo"action='registro.php?code=".$_GET['code']."'" ?> method="post" accept-charset="utf-8" class="mt-5">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Nome</label>
                            <input type="text" name="nome" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="O Seu Nome " <?php echo"value='".$nome."'" ?> >
                        </div>
                          <div class="form-group">
                            <label for="exampleFormControlInput1">SobreNome</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Seu Sobrenome" name="sobrenome" <?php echo"value='".$sobrenome."'" ?>>
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlInput1">Sua data de nascimento</label>
                            <input type="date" class="form-control" name="data_nascimento1" <?php echo"value='".$data1."'" ?>>
                          </div>
                          <div class="form-group">
                              <label for="exampleFormControlInput1">Cor Do Cabelo</label>
                            <input type="text" class="form-control" name="cabelo_pessoa1" placeholder="Sua cor de cabelo"<?php echo"value='".$cabelo."'" ?>>
                          </div>
                          
                          <div class="form-group">
                              <label for="exampleFormControlInput1">Estado</label>
                            <select id="sd" class="form-control" name="estado">
                            </select>    
                          </div>
                          
                          <div class="form-group">
                              <label for="exampleFormControlInput1">Cidade</label>
                            <select id="ci" class="form-control" name="cidade" >
                            </select>    
                          </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Nome De Usuario</label>
                            <input type="text" name="nome_usuario" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nome de usuario" required="" <?php echo"value='".$nome_usuario."'" ?>>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Senha</label>
                            <input type="password" name="senha" class="form-control" id="exampleInputPassword1" placeholder="Senha" required="">
                        </div>
                        <div class="form-group">
                            
                            <label for="exampleFormControlInput1">Senha Novamente</label>
                            <input type="password" name="senha_novamente" class="form-control" id="exampleInputPassword1" placeholder="Senha Novamente" required="">
                        </div>
                        <button type="submit" class="btn btn-danger btn-sm">Cadastrar</button>
                    </form>
                </div>

            </div>


        </div>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>

</html>