<?php 
include("config/header.php");
$sql="SELECT * FROM tb_user where id = ".$_SESSION['id'];
$res=mysqli_query($linkdeconexao_db,$sql);
$row=mysqli_fetch_array($res);
?>
<!doctype html>
<html lang="pt-br">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="/img/icon.png">
<title>PrivêWS</title>

<!-- Principal CSS do Bootstrap -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">

<!-- Estilos customizados -->
<link href="/dist/css/dashboard.css" rel="stylesheet">
<link href="/dist/css/theme.css" rel="stylesheet">
<link href="/dist/css/jquery.privews.min.css" rel="stylesheet">
<link href="/fancybox/jquery.fancybox.min.css" rel="stylesheet">
<link href="/select2/select2.min.css" rel="stylesheet">
<script type="text/javascript">
		$(document).ready(function(){
		    
$('#casal_solteiro').on("change", function(){
    var value=document.getElementById("casal_solteiro").value;
  if(value =="Casal")
  {
    document.getElementById("casal").style.display = 'block';
  }
  else	if(value == "Solteiro")
  {
      document.getElementById("casal").style.display = 'none';
  }
});
});
</script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.form.min.js"></script>
<script type="text/javascript">
// foto perfil
$(document).ready(function () {
    $('#submitButton').click(function () {
            $('#uploadForm').ajaxForm({
                target: '#outputImage',
                url: 'upload_imagem.php?perfil',
                beforeSubmit: function () {
                      $("#outputImage").hide();
                       if($("#uploadImage").val() == "") {
                           $("#outputImage").show();
                           $("#outputImage").html("<div class='error'>Choose a file to upload.</div>");
                    return false; 
                }
                    $("#progressDivId").css("display", "block");
                    
                },
                uploadProgress: function (event, position, total, percentComplete) {

                },
                error: function (response, status, e) {
                    alert('Oops something went.');
                },
                complete: function (xhr) {
                    console.log("resposta:"+xhr.responseText);
                    if (xhr.responseText != "error")
                    {
                           $("#progressDivId").css("display", "none");
                           $("#ok_imagem").html("<div class='alert alert-success' role='alert'>Salvo Com Sucesso.</div>");
                           $('#meu_perfil').modal('hide');
                    }
                    else if (xhr.responseText == "error")
                    {  
                           $("#progressDivId").css("display", "none");
                           $("#ok_imagem").html(responseText);
                           $('#meu_perfil').modal('hide');
                    }
                    else
                    {
                         alert("Erro");
                    }
                }
            });
    });
});
// editar
$(document).ready(function () {
    $('#submitPerfil').click(function () {
            $('#PerfilForm').ajaxForm({
                url: 'editar_dados_perfil.php',
                beforeSubmit: function () 
                {
                    $("#progressDivPerfil").css("display", "block");
                },
                uploadProgress: function (event, position, total, percentComplete) {

                },
                error: function (response, status, e) {
                    alert('Oops something went.');
                },
                complete: function (xhr) {
                    console.log("resposta:"+xhr.responseText);
                    alert(xhr.responseText);
                    if (xhr.responseText != "error")
                    {
                           $("#progressDivPerfil").css("display", "none");
                           $("#ok_imagem").html("<div class='alert alert-success' role='alert'>Salvo Com Sucesso.</div>");
                           $('#').modal('hide');
                    }
                    else if (xhr.responseText == "error")
                    {  
                           $("#progressDivPerfil").css("display", "none");
                           $("#ok_imagem").html(responseText);
                           $('#meu_perfil').modal('hide');
                    }
                    else
                    {
                         alert("Erro");
                    }
                }
            });
    });
});
// convite
$(document).ready(function () {
    $('#submitConvite').click(function () {
            $('#ConviteForm').ajaxForm({
                url: 'convite.php',
                beforeSubmit: function () 
                {
                    $("#progressDivPerfil").css("display", "block");
                },
                uploadProgress: function (event, position, total, percentComplete) {

                },
                error: function (response, status, e) {
                    alert('Oops something went.');
                },
                complete: function (xhr) {
                    console.log("resposta:"+xhr.responseText);
                    alert(xhr.responseText);
                    if (xhr.responseText != "error")
                    {
                           $("#progressDivConvite").css("display", "none");
                           $("#ok_imagem").html("");
                           $('#exampleModal').modal('hide');
                    }
                    else if (xhr.responseText == "error")
                    {  
                           $("#progressDivConvite").css("display", "none");
                           $("#ok_imagem").html(responseText);
                           $('#exampleModal').modal('hide');
                    }
                    else
                    {
                         alert("Erro");
                    }
                }
            });
    });
});
//senha
$(document).ready(function () {
    $('#submitSenha').click(function () {
        alert("Senha.");
            $('#SenhaAForm').ajaxForm({
                url: 'trocar_senha.php?sabe',
                beforeSubmit: function () 
                {
                    $("#progressDivSenha").css("display", "block");
                },
                uploadProgress: function (event, position, total, percentComplete) {

                },
                error: function (response, status, e) {
                    alert('Oops something went.');
                },
                complete: function (xhr) {
                    if (xhr.responseText=="Senha Confere")
                    {
                           $("#progressDivSenha").css("display", "none");
                           $('#senhaConfere').modal('show');
                    }
                    else if (xhr.responseText == "Senha Nao Confere")
                    {  
                           $("#progressDivSenha").css("display", "none");
                           alert("Senha Não Confere.");
                    }
                    else
                    {
                         alert("Erro");
                    }
                }
            });
    });
});
//confere
$(document).ready(function () {
    $('#submitConfere').click(function () {
            $('#SenhaConfere').ajaxForm({
                url: 'trocar_senha.php?trocar',
                beforeSubmit: function () 
                {
                    $("#progressDivSenha").css("display", "block");
                },
                uploadProgress: function (event, position, total, percentComplete) {

                },
                error: function (response, status, e) {
                    alert('Oops something went.');
                },
                complete: function (xhr) {
                    if (xhr.responseText=="ok")
                    {
                           $("#progressDivSenha").css("display", "none");
                           $("#senha_ok").html('<div class="alert alert-success" role="alert">Senha Trocada Com Sucesso.</div>');
                           $('#senhaConfere').modal('hide');
                           $('#senhaAtual').modal('hide');
                    }
                    else if (xhr.responseText == "Senha Igual A Antiga")
                    {  
                           $("#progressDivSenha").css("display", "none");
                           $("#senha_ok").html('<div class="alert alert-danger" role="alert">Senha Igual A Antiga</div>');
                    }
                    else
                    {
                         alert("Erro");
                    }
                }
            });
    });
});

enviar_imagem = function()
{
    $("#ar_imagem").click();
}
preview_imagens = function(input)
{
    if(input.files && input.files[0]) 
    {
                    var reader = new FileReader();
                    reader.onload = function(e) 
                    {
                        $('#img_perfil').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                    $('#meu_perfil').modal('show');
    }
}
</script>
</head>
<body>
    <div id="ok_imagem"></div>
<!-- ===============================================--> 
<!--    Main Content--> 
<!-- ===============================================--> 
<!--main class="main" id="top"-->
<div class="container">
    
	
	
<main class="container">
    <div class="container">
    <?php include("config/barra_lateral.php");?>
        <div class="content">
            <nav class="navbar navbar-light navbar-glass fs--1 font-weight-semi-bold row  navbar-expand">
  <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarVerticalCollapse" aria-controls="navbarVerticalCollapse" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span></button>
</nav>
            <div class="row no-gutters">
              <div class="col-lg-8 pr-lg-2"> 
                
                <!-- ===============================
            			Minhas Coisas
            		   =============================== -->
                
                <div class="card">
                  <div class="card-header bg-light">
                    <div class="row align-items-center">
                      <div class="col-12">
                        <h4 class="h3 text-danger text-center mb-0" id="followers">MINHAS COISAS</h4>
                      </div>
                      <div class="col"> </div>
                    </div>
                  </div>
                  <div class="card-body bg-light p-0">
                    <div class="row no-gutters text-left fs--1">
                      <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
                        <div class="bg-white pt-3 h-100"><a href="#"></a>
                          <h4 class="h5 mb-1 ml-3"><a  href="#"><span class="far fa-envelope"></span><span class="text-dark"> Meus Recados</span></a></h4>
                        </div>
                      </div>
                      <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
                        <div class="bg-white pt-3 h-100"><a href="#"></a>
                          <h4 class="h5 mb-1 ml-3"><a  href="\amizades.php"><span class="fas fa-user-friends"></span><span class="text-dark"> Meus Amigos</span></a></h4>
                        </div>
                      </div>
                      <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
                        <div class="bg-white pt-3 h-100"><a href="#"></a>
                          <h4 class="h5 mb-1 ml-3"><a  href="\ranking.php"><span class="fas fa-star"></span><span class="text-dark"> Ranking Top SW</span></a></h4>
                        </div>
                      </div>
                      <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
                        <div class="bg-white pt-3 h-100"><a href="#"></a>
                          <h4 class="h5 mb-1 ml-3"><a  href="#"><span class="fas fa-dollar-sign"></span><span class="text-dark"> Pagamento</span></a></h4>
                        </div>
                      </div>
                      <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
                        <div class="bg-white pt-3 h-100"><a href="#"></a>
                          <h4 class="h5 mb-1 ml-3"><a  href="\minhas_paqueras.php"><span class="fas fa-heart"></span><span class="text-dark">Minhas Paqueras</span></a></h4>
                        </div>
                      </div>
                      <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
                        <div class="bg-white pt-3 h-100">
                          <h4 class="h5 mb-1 ml-3">
                                <?php 
                                if(mysqli_num_rows($res_diplomata)>=3 || $row['adm']>0){?>  
                                <a href="#" data-toggle="modal" data-target="#exampleModal" >
                                    <span class="fas fa-heart"></span><span class="text-dark">Convidar Amigos</span>
                                </a>
                                <?php }else{?>
                                    <a tabindex="0"  role="button" data-toggle="popover" data-trigger="focus" title="Você Nâo Tem Certificado De Diplomata." data-content="Você Nâo Tem Certificado De Diplomata.">
                                        <span class="fas fa-heart"></span><span class="text-dark">Convidar Amigos</span>
                                    </a>
            
                                    <?php }?>
                            </h4>    
                        </div>
                      </div>
                      <div class="col-12">
                        <h4 class="text-danger text-center m-4" id="followers">Ferramentas</h4>
                      </div>
          <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
            <div class="bg-white pt-3 h-100"><a href="#"></a>
              <h4 class="h5 mb-1 ml-3"><a href="#"><span class="fas fa-check-double"></span> <span class="text-dark"> Siga-me</span></a></h4>
            </div>
          </div>
          <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
            <div class="bg-white pt-3 h-100"><a href="#"></a>
              <h4 class="h5 mb-1 ml-3"><a href="#"><span class="fas fa-sync-alt"></span> <span class="text-dark"> Últimas Atualizações</span></a></h4>
            </div>
          </div>
          <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
            <div class="bg-white pt-3 h-100"><a href="#"></a>
              <h4 class="h5 mb-1 ml-3"><a href="/forum/forum.php"><span class="far fa-comment-dots"></span><span class="text-dark"> Fórum</span></a></h4>
            </div>
          </div>
          <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
            <div class="bg-white pt-3 h-100"><a href="#"></a>
              <h4 class="h5 mb-1 ml-3"><a href="#"><span class="fas fa-th-list"></span><span class="text-dark"> Grupos</span></a></h4>
            </div>
          </div>
          <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
            <div class="bg-white p-3 h-100"><a href="#"></a>
              <h4 class="h5"><a href="#"><span class="fas fa-map-marker-alt"></span><span class="text-dark"> Radar</span></a></h4>
            </div>
          </div>
          <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
            <div class="bg-white p-3 h-100"><a href="#"></a>
              <h4 class="h5"><a href="#"><span class="far fa-calendar-alt"></span><span class="text-dark"> Eventos</span></a></h4>
            </div>
          </div>
                      <div class="col-12">
                        <h4 class="text-danger text-center m-4" id="followers">CONFIGURAÇÕES</h4>
                      </div>
                      <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
                        <div class="bg-white pt-3 h-100"><a href="#"></a>
                          <h4 class="h5 ml-3">
                              <a href="/seu_perfil.php">
                                <span class="far fa-edit"></span><span class="text-dark">Editar Perfil</span>
                              </a>
                          </h4>
                        </div>
                      </div>
                      <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
                        <div class="bg-white pt-3 h-100"><a href="#"></a>
                          <h4 class="h5"><a href="#"  data-toggle="modal" data-target="#senha_atual" ><span class="fas fa-key"></span><span class="text-dark"> Trocar Senha</span></a></h4>
                        </div>
                      </div>
                      <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
                            <div class="bg-white pt-3 h-100"><a href="#"></a>
                              <h4 class="h5 ml-3"><a href="#"><span class="fab fa-expeditedssl"></span><span class="text-dark"> Privacidade e Segurança</span> </a></h4>
                            </div>
                          </div>
                          <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
                            <div class="bg-white pt-3 h-100"><a href="#"></a>
                              <h4 class="h5"><a href="#"><span class="fas fa-question-circle"></span><span class="text-dark"> Fale Conosco</span></a></h4>
                            </div>
                          </div>
                          <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
                            <div class="bg-white p-3 h-100"><a href="\sair.php"></a>
                              <h4 class="h5"><a href="#"><span class="fas fa-window-close"></span><span class="text-dark"> Sair</span></a></h4>
                            </div>
                          </div>
                          <div class="col-6 col-md-4 col-lg-6 col-xxl-2 mb-1">
                            <div class="bg-white p-3 h-100"><a href="#"></a>
                              <h4 class="h5"><a href="#"><span class="fas fa-user-lock"></span><span class="text-dark"> Acesso Rápido</span></a></h4>
                            </div>
                          </div>
                    </div>
                  </div>
                </div>
                
                <!-- ===============================
            				fim Minhas Coisas
            		   =============================== --> 
                
              </div>
              <?php include("config/barra_ranking.php");?>
               <!-- 
            		===========================================
            						convite 
            		===========================================
            		-->
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Convidar Amigo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                   <div id="progressDivConvite" style="display:none">
                        <img src="/file/img_padrao/loading.gif" width="80" heigth="80">
                    </div>
                        <form method='post' id="ConviteForm">
                            <input type="hidden" name="token" <?php echo"value='".base64_encode($id)."'"?>>
                            <div class="modal-body">
                                <div class="form-group">
                                        <label for="exampleInputEmail1">Endereço De Email Do Seu Amigo</label>
                                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Seu email">
                                      </div>
                                <div class="form-group">
                                          <span class="genero-pessoa1">Como Você Conhece O Seu Amigo(a)</span>
                                    			<select class="custom-select"  name="conhece-real-ou-virtual">
                                                    <option value="real">Pessoalmente</option>
                                                    <option value="virtual">Virtualmente</option>
                                    			</select>
                                      </div>
                                <div class="form-group">
                                          <span class="genero-pessoa1">Qual o genero do(a) seu amigo(a)?</span>
                                    			<select name="genero-pessoa1" class="custom-select" >
                                                    <?php
                                                    
                                                    $sql_genero="SELECT * FROM `tb_genero`";
                                                    $res_genero=mysqli_query($linkdeconexao_db,$sql_genero);
                                                    $linhas=array();
                                                    $i=0;
                                                    while($row_genero=mysqli_fetch_array($res_genero))
                                                    {
                                                        echo"<option value='".$row_genero['id']."'>".$row_genero['Genero']."</option>";
                                                        $linhas[$i]=array('id'=>$row_genero['id'],'Genero'=>$row_genero['Genero']);
                                                        $i++;
                                                    }
                                                    ?>
                                    			</select>
                                      </div>
                                <div class="form-group">
                                          <span >Casal ou Solteiro</span>
                                    			<select class="custom-select"  id="casal_solteiro" name="casal-solteiro">
                                                   <option value='Solteiro'>Solteiro</option>
                                                   <option value='Casal'>Casal</option>
                                    			</select>
                                      </div>
                                <div class="form-group" id="casal" style="display:none;">
                                           <span >Genero Do(a) Parceiro Do Seu Amigo</span>
                                    			<select class="custom-select"  name="genero-pessoa2">
                                                    <?php
                                                    $i=0;
                                                    while($i<sizeof($linhas))
                                                    {
                                                        echo"<option value='".$linhas[$i]['id']."'>".$linhas[$i]['Genero']."</option>";
                                                        $i++;
                                                    }
                                                    ?>
                                    			</select>
                                      </div>
                            </div>
                            <div class="modal-footer">
                                 <button type="submit" class="btn btn-primary" id="submitConvite">Enviar</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fecha</button>
                            </div>  
                        </form>
                    </div>
                </div>
              </div>   
               <!-- 
            		===========================================
            						mudar foto 
            		===========================================
            		-->
              <div class="modal" id="meu_perfil" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Mudar Foto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                   <div id="progressDivId" style="display:none">
                        <img src="/file/img_padrao/loading.gif" width="80" heigth="80">
                    </div>
                    <div id="publicacao_ok"></div>
                  <form id="uploadForm" name="frmupload" method="post" enctype="multipart/form-data" >
                  <div class="modal-body">
                    <p>Tem Certeza Que deseja Alterar</p>
                    <div class="invisible">
                    <input type="file" name="arquivo" id="ar_imagem" accept="image/*" onchange="preview_imagens(this)">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
                    <button id="submitButton" type="submit" class="btn btn-primary">Sim</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
             <!-- 
            		===========================================
            						senha_atual
            		===========================================
            		-->
              <div class="modal" id="senha_atual" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Mudar Senha</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                   <div id="progressDivSenha" style="display:none">
                        <img src="/file/img_padrao/loading.gif" width="80" heigth="80">
                    </div>
                    <div id="senhaT_ok"></div>
                    <?php
                        $sql="SELECT * FROM tb_user where id = ".$_SESSION['id'];
                        $res=mysqli_query($linkdeconexao_db,$sql);
                        $row=mysqli_fetch_array($res);
                        ?>
                  <form  method="post" id="SenhaAForm">
                  <div class="modal-body">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Digite A Senha Atual</label>
                                        <input type="password" name="senha" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Digite Aqui A Sua Atual Senha " <?php echo"value='".$row['nome']."'" ?> >
                                    </div>
                                    <input name="token" type="hidden" <?php echo" value='".base64_encode($row['id'])."'";?>>
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
                    <button id="submitSenha" type="submit" class="btn btn-primary">Sim</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
            
            
            
             <!-- 
            		===========================================
            						senha_confere
            		===========================================
            		-->
              <div class="modal" id="senhaConfere" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Mudar Senha</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                   <div id="progressDivSenhaC" style="display:none">
                        <img src="/file/img_padrao/loading.gif" width="80" heigth="80">
                    </div>
                    <div id="senha_ok"></div>
                  <form  method="post" id="SenhaConfere">
                  <div class="modal-body">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Digite A Sua Nova Senha</label>
                                        <input type="password" name="senha1" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Digite Aqui A Sua Atual Senha " <?php echo"value='".$row['nome']."'" ?> >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Digite A Sua Senha Novamente</label>
                                        <input type="password" name="senha2" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Digite Aqui A Sua Atual Senha " <?php echo"value='".$row['nome']."'" ?> >
                                    </div>
                                    <input name="token" type="hidden" <?php echo"value='".base64_encode($row['id'])."'"; ?> >
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
                    <button id="submitConfere" type="submit" class="btn btn-primary">Sim</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
              <!-- 
            		===========================================
            						sidebar 
            		===========================================
            		-->
            
<!--/main--> 
</div>
        </div>
    </div>
</main>    
<!-- ===============================================--> 
<!--    End of Main Content--> 
<!-- ===============================================--> 

<!-- Principal JavaScript do Bootstrap
    ================================================== --> 
<!-- Foi colocado no final para a página carregar mais rápido --> 

<script src="/assets/js/vendor/popper.min.js"></script> 
<script src="/dist/js/bootstrap.min.js"></script> 
<script src="/dist/js/theme.js"></script> 
<script src="/fancybox/jquery.fancybox.min.js"></script> 
<script src="/select2/select2.min.js"></script> 
<script src="/@fortawesome/all.min.js"></script>
</body>
</html>
