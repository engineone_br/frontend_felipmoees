<?php 
session_start();
include("config/header.php");
$sql="SELECT * FROM tb_user where id = ".$_SESSION['id'];
$res=mysqli_query($linkdeconexao_db,$sql);
$row=mysqli_fetch_array($res);
$sql_amigos_total="SELECT u.img as imagem_de, u.usuario as usuario_de,u.id as id_de,u1.id as id_para ,u1.img as imagem_para,u1.usuario as usuario_para FROM `tb_amizades` as a 
INNER JOIN tb_user as u on u.id=a.de 
INNER JOIN tb_user as u1 on u1.id=a.para
where (a.de = ".$_SESSION['id']." ) or (a.para = ".$_SESSION['id'].")";
$res_amigos_total=mysqli_query($linkdeconexao_db,$sql_amigos_total);
$total=mysqli_num_rows($res_amigos_total);
$sql_amigos="SELECT u.img as imagem_de, u.usuario as usuario_de,u.id as id_de,u1.id as id_para ,u1.img as imagem_para,u1.usuario as usuario_para FROM `tb_amizades` as a 
INNER JOIN tb_user as u on u.id=a.de 
INNER JOIN tb_user as u1 on u1.id=a.para
where (a.de = ".$_SESSION['id']." ) or (a.para = ".$_SESSION['id'].") Limit 10";
$res_amigos=mysqli_query($linkdeconexao_db,$sql_amigos);
?>
<!doctype html>
<html lang="pt-br">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="img/icon.png">
<title>PrivêWS</title>
<!-- Principal CSS do Bootstrap -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Estilos customizados -->
<link href="dist/css/dashboard.css" rel="stylesheet">
<link href="dist/css/theme.css" rel="stylesheet">
<link href="dist/css/jquery.privews.min.css" rel="stylesheet">
<link href="fancybox/jquery.fancybox.min.css" rel="stylesheet">
<link href="select2/select2.min.css" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.form.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
 $(window).scroll(function(){
  var position = $(window).scrollTop();
  var bottom = $(window).height()-$(document).height();
  if( position == bottom )
  {
   var row = Number($('#row').val());
   var allcount = Number($('#all').val());
   var rowperpage = 4;
   row = row + rowperpage;
   if(row <= allcount)
   {
        $('#row').val(row);
        $.ajax({
         url: '/atualizar_amizades.php',
         type: 'post',
         data: {row:row},
         success: function(response)
         {
            $("#conteudo").html(response);
         },
         error: function(response)
         {
             alert("Erro:"+response);
         }
        });
   }
   else if(row == allcount)
   {
       $("#inicial").clear();
       $('#row').val("0");
   }
  }

 });
 
});
</script>
</head>

<body>
<!-- ===============================================--> 
<!--    Main Content--> 
<!-- ===============================================--> 
<!--main class="main" id="top"-->
<div class="container">
<main class="container">
<div class="container">
<?php include("config/barra_lateral.php");?>
    <div class="content">
<nav class="navbar navbar-light navbar-glass fs--1 font-weight-semi-bold row  navbar-expand">
  <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarVerticalCollapse" aria-controls="navbarVerticalCollapse" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span></button>
</nav>
<div class="row no-gutters">
  <div class="col-lg-8 pr-lg-2"> 
    
    <!-- ===============================
			Amigos
		   =============================== -->
    
    <div class="card">
      <div class="card-header bg-light">
        <div class="row align-items-center">
          <div class="col-12">
            <h4 class="h3 text-danger text-center mb-0" id="followers">MEUS AMIGOS</h4>
          </div>
        </div>
      </div>
      <div class="card-body bg-light p-0">
        <div class="row">
                <?php if(mysqli_num_rows($res_amigos)>0)
                        {
                            while($row_amigos=mysqli_fetch_array($res_amigos))
                            {
                                if($row_amigos['id_para'] == $id)
                                {
                                    $usuario=$row_amigos['usuario_de'];
                                    $imagem=$row_amigos['imagem_de'];
                                }
                                else if($row_amigos['id_de'] == $id)
                                {
                                    $usuario=$row_amigos['usuario_para'];
                                    $imagem=$row_amigos['imagem_para'];
                                }
                            ?>    
                                <div class="col-6">
                                    <div class="bg-white pt-4 h-300">
                                        <center>
                                            <img class="img-thumbnail" <?php echo"src='".converter_imagem($imagem)."'" ?> alt="" /> 
                                            <div class="media-body align-self-center ml-2">
                                                <p class="mb-1 line-height-1"><a class="font-weight-semi-bold" <?php echo"".converter_link($usuario)."" ?>>
                                                   <?php echo"".converter_usuario($usuario)."" ?></a></p>
                                            </div>
                                    </div></center>
                                </div>
                <?php       }
                        }?>    
                <input type="hidden" id="row" value="10">
                <input type="hidden" id="all" <?php echo"value='".$total."'"?>>
                <div id="conteudo"></div>
        </div>
        </div>
      </div>
    <!-- ===============================
				fim Meus Amigos
		   =============================== --> 
    
  </div>
  
  <!-- 
		===========================================
						sidebar 
		===========================================
		-->

<!--/main--> 

<!-- ===============================================--> 
<!--    End of Main Content--> 
<!-- ===============================================--> 

<!-- Principal JavaScript do Bootstrap
    ================================================== --> 
<!-- Foi colocado no final para a página carregar mais rápido --> 

<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-slim.min.js"><\/script>')</script> 
<script src="assets/js/vendor/popper.min.js"></script> 
<script src="dist/js/bootstrap.min.js"></script> 
<script src="dist/js/theme.js"></script> 
<script src="dist/js/jquery.min.js"></script> 
<script src="fancybox/jquery.fancybox.min.js"></script> 
<script src="select2/select2.min.js"></script> 
<script src="@fortawesome/all.min.js"></script>
</body>
</html>
