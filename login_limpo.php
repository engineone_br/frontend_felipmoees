<?php
session_start();
include("config/db.php");
if(isset($_SESSION['usuario']['email']))
{
	header("Location: /dashboard.php");
}
else
{?>

<!doctype html>
<html lang="pt-br">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Gugi|Roboto:400,700" rel="stylesheet">

    <title>Rede Social</title>
    <style>
        html,
        body {
            height: 100%;
        }

        body {
            font-family: 'Roboto', sans-serif;
            height: 100%;

        }

        .agendo {
            font-family: 'Gugi', cursive;
        }

        .f16 {
            font-size: 16px;
        }

        .linha0 {
            line-height: 0.9;
        }

        .container-fluid {
            height: 100%;
        }

        .fundo_login {
            background-image: url('/assets/imgs/fundo.png');
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>

<body>
    
<?php
	if(isset($_POST['email']) && isset($_POST['senha'])) {
		$email = $_POST['email'];
		$senha = $_POST['senha'];
		$verifica = mysqli_query($linkdeconexao_db, "SELECT * FROM tb_user WHERE email = '$email' AND senha ='$senha'");
		if (mysqli_num_rows($verifica)>0) {
		    $_SESSION['usuario']['email']=$email;
			header("Location: /dashboard.php");
		}
		else
		{
			echo "<div class='alert alert-danger' role='alert'>E-mail e Senha não conferem!</div>";
		}
	}
}
?>
    <div class="container-fluid">

        <div class="row" style="height: 100%;">

            <div class="col-md-6 col-sm-12 col-xs-12 pl-5 pr-5 pb-3" style="background-image: linear-gradient(to bottom, purple, black); height: 100%; color:#fff;">
                <!-- Login -->

                <div class="col-md-8 offset-md-2 col-sm-12 offset-sm-0 col-xs-12 offset-xs-0 mt-5">
                    <br>
                    <p class="h5 linha0 mt-5"><span class="font-weight-bold">Seja Muito Bem-Vindo(a) à Privê SW</span><br><small class="f16">Efetue seu Login para Acessar</small></p>

                    <form action="index.php" method="post" accept-charset="utf-8" class="mt-5">
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="E-mail" required="">
                        </div>
                        <div class="form-group">
                            <input type="password" name="senha" class="form-control" id="exampleInputPassword1" placeholder="Senha" required="">
                        </div>

                        <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-angle-right"></i> Acessar Meu Perfil</button>
                        <br>
                        <br>

                        <label class="checkbox pull-left">
                            <input type="checkbox" value="remember-me">
                            Manter Conectado
                        </label>
                    </form>
                </div>

            </div>


        </div>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>

</html>