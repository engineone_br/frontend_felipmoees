<?php
include("config/db.php");
include("config/getdados.php");
if(isset($_POST['token']))
{
    $id=base64_decode($_POST['token']);
    $saberrperfil = mysqli_query($linkdeconexao_db, "SELECT * FROM tb_user WHERE id = $id");
    if(mysqli_num_rows($saberrperfil)>0)
    {
        if(isset($_POST['texto']))
        {
            if(strlen($_POST['texto'])>4)
            {
                date_default_timezone_set('America/Sao_Paulo');
                $data = date('Y-m-d H:i:s');
                $ultimo=0;
                if(isset($_FILES['fotos']['name']))
                {
                        $link=array();
                        $tamanho=$_POST['tamanho'];
                        for($i = 0; $i < $tamanho; $i++)
                        {  
                            if($_FILES['fotos']['size'][$i] < 5242880)
                            {
                                $arquivo_tmp = $_FILES['fotos']['tmp_name'][$i];
                                $nome = $_FILES['fotos']['name'][$i];
                                // Pega a extensão
                                $extensao = pathinfo($nome, PATHINFO_EXTENSION );
                                // Converte a extensão para minúsculo
                                $extensao = strtolower($extensao);
                                // Somente imagens, .jpg;.jpeg;.gif;.png
                                // Aqui eu enfileiro as extensões permitidas e separo por ';'
                                // Isso serve apenas para eu poder pesquisar dentro desta String
                                if (strstr('.jpg;.jpeg;.gif;.png',$extensao)) 
                                {
                        				// Cria um nome único para esta imagem
                        				// Evita que duplique as imagens no servidor.
                        				$novoNome = time();
                        				// Concatena a pasta com o nome
                        				$destino ="file/img_post/".$novoNome; 
                        				$destino1 ="file/img_post/".$novoNome.".png";
                                		// tenta mover o arquivo para o destino
                                		imagepng(imagecreatefromstring(file_get_contents($arquivo_tmp)),$destino1);
                                		$width = 400;
                                		$height = 400;		
                                       // O resize propriamente dito. Na verdade, estamos gerando uma nova imagem.
                                       $imagem = imagecreatetruecolor($width, $height);
                                       $image = imagecreatefrompng($destino1);
                                		// Obtendo o tamanho original
                                		$width_orig = imagesx($image);
                                		$height_orig = imagesy($image);
                                       imagecopyresampled($imagem, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
                                       // Ou, se preferir, Salvando a imagem em arquivo:
                                       imagepng($imagem,$destino1);
                                       
                    		            $imagemLogo = imagecreatefrompng("config/logo.png");
                                        
                                        // Copia a logo para a imagem
                                        imagecopymerge( $imagem, $imagemLogo,0,350,0,0,400,50,8);
                                        imagepng($imagem,$destino1);
                        			if(move_uploaded_file ( $arquivo_tmp, $destino ) ) 
                        			{
                        				unlink($destino);   
                        				array_push($link,$destino1);
                        			}
                                    else
                                    {
                         		        echo "<div class='alert alert-danger' role='alert'>Erro ao salvar o arquivo. Aparentemente você não tem permissão de escrita</div>";
                        		    }
                                    }  
                                else
                                {
                        		        echo "<div class='alert alert-danger' role='alert'>Você poderá enviar apenas arquivos *.jpg;*.jpeg;*.gif;*.png</div>";
                                }
                            }    
                        }
                        $texto=array('Imagens'=>$link,'Videos'=>null,'Texto'=>utf8_encode($_POST['texto']));
                        $texto=json_encode($texto,JSON_UNESCAPED_UNICODE);
                        $sql="INSERT INTO tb_pubs(usuario,texto,data)VALUES($id,'$texto','$data')";
                        if(mysqli_query($linkdeconexao_db,$sql))
                        {
                            $ultimo=mysqli_insert_id($linkdeconexao_db);
                            echo "<div class='alert alert-success' role='alert'>Publicado Com Sucesso.</div>";
                        }
                        else
                        {
                            echo"<div class='alert alert-danger' role='alert'>Erro Ao Publicar </div>";
                        } 
                }
                else if(isset($_FILES['videos']['name']))
                {
                    if($_FILES['videos']['size'] < 15242880)
                    {
                                $link=array();
                                $arquivo_tmp = $_FILES['videos']['tmp_name'];
                                $nome = $_FILES['videos']['name'];
                                // Pega a extensão
                                $extensao = pathinfo($nome, PATHINFO_EXTENSION );
                                // Converte a extensão para minúsculo
                                $extensao = strtolower($extensao);
                                // Somente imagens, .jpg;.jpeg;.gif;.png
                                // Aqui eu enfileiro as extensões permitidas e separo por ';'
                                // Isso serve apenas para eu poder pesquisar dentro desta String
                                if (strstr('.mp4;.avi;.mkv;',$extensao)) 
                                {
                        			$novoNome = time(); 
                        			$destino1 ="file/video_post/".$novoNome.".".$extensao;
                        			if(move_uploaded_file($arquivo_tmp,$destino1)) 
                        			{  
                        			    $ultimo=mysqli_insert_id($linkdeconexao_db);
                        				array_push($link,$destino1);
                        			}
                                    else
                                    {
                         		        echo "<div class='alert alert-danger' role='alert'>Erro ao salvar o arquivo. Aparentemente você não tem permissão de escrita</div>";
                        		    }
                                }  
                                else
                                {
                        		        echo "<div class='alert alert-danger' role='alert'>Você poderá enviar apenas arquivos *.jpg;*.jpeg;*.gif;*.png</div>";
                                }
                            }   
                    else
                    {
                        echo"<div class='alert alert-danger' role='alert'>Video muito grande</div>";
                    }
                    $texto=array('Imagens'=>null,'Videos'=>$link,'Texto'=>$_POST['texto']);
                    $texto=json_encode($texto,JSON_UNESCAPED_UNICODE);
                    $sql="INSERT INTO tb_pubs(usuario,texto,data)VALUES($id,'$texto','$data')";
                    if(mysqli_query($linkdeconexao_db,$sql))
                    {
                        unset($_POST['texto']);
                        echo "<div class='alert alert-success' role='alert'>Publicado Com Sucesso.</div>";
                    }
                    else
                    {
                        echo"<div class='alert alert-danger' role='alert'>Erro Ao Publicar </div>";
                    }
                                
                }
                else
                {   
                    $texto=array('Imagens'=>$link,'Videos'=>null,'Texto'=>$_POST['texto']);
                    $texto=json_encode($texto,JSON_UNESCAPED_UNICODE);
                    $sql="INSERT INTO tb_pubs(usuario,texto,data)VALUES($id,'$texto','$data')";
                    if(mysqli_query($linkdeconexao_db,$sql))
                    {
                          echo "<div class='alert alert-success' role='alert'>Publicado Com Sucesso.</div>";
                          $ultimo=mysqli_insert_id($linkdeconexao_db);
                    }
                    else
                    {
                        echo"<div class='alert alert-danger' role='alert'>Erro Ao Publicar </div>";
                    }
                }
                if($ultimo!=0)
                {
                    $sql="SELECT p.id_pubs,p.usuario,p.texto,p.data,u.id,u.usuario,u.img FROM `tb_pubs` as p 
                    INNER JOIN tb_user as u on p.usuario=u.id where p.id_pubs = $ultimo";
                    $res=mysqli_query($linkdeconexao_db,$sql);
                    $img="";
                    if($row=mysqli_fetch_array($res))
                    {
                    $img=$row['img'];    
                    ?>
                        <div class="card mb-3">
                            <div class="card-header bg-light">
                                <div class="row justify-content-between">
                                  <div class="col">
                                    <div class="media">
                                      <div class="avatar avatar-2xl"> <img class="rounded-circle" <?php echo"src='".converter_imagem($row['img'])."'" ?> alt="" /> </div>
                                      <div class="media-body align-self-center ml-2">
                                        <p class="mb-1 line-height-1">
                                            <div class="col-8">
                                                <a class="font-weight-semi-bold" <?php echo"href='/perfil.php?usuario=".$row['usuario']."'";?> > 
                                                <?php echo"".converter_usuario($row['usuario']); ?> </a>
                                            </div>
                                            <div class="col-4">
                                                <?php echo"".opcoes_publicacao($row['id'],$row['id_pubs']); ?>
                                            </div>
                                        </p>
                                        <p class="mb-0 fs--1"><?php echo"".converter_data($row['data']); ?></p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="card-body overflow-hidden">
                            <p><?php echo"".converter_texto($row['texto']); ?> </p>
                                    <?php 
                                    $link_imagens=converter_post_imagens($row['texto']);
                                    $link_video=converter_post_video($row['texto']);
                                    if($link_imagens[0]!=1){
                                            if(sizeof($link_imagens)<2){?>
                                    <div class=" p-1"> 
                                        <a <?php echo"href='".$link_imagens[0]."'" ?> data-fancybox="" data-caption="descrição de imagen">
                                        <img class="rounded w-100" <?php echo"src='".$link_imagens[0]."'" ?> alt=""></a> 
                                    </div>
                                    <?php }else if(sizeof($link_imagens)>1){?>
                                        <div class="row mx-n1">
                                            <div class="col-6 p-1">
                                                <a <?php echo"href='".$link_imagens[0]."'" ?> data-fancybox="gallery" data-caption="Image caption">
                                                <img class="rounded w-100" <?php echo"src='".$link_imagens[0]."'" ?> alt=""></a>
                                            </div>
                                            <div class="col-6 p-1">
                                                <a <?php echo"href='".$link_imagens[1]."'" ?> data-fancybox="gallery" data-caption="Image caption">
                                                <img class="rounded w-100" <?php echo"src='".$link_imagens[1]."'" ?> alt="">
                                                </a>
                                            </div>
                                        </div>
                                    <?php }else {
                                                
                                        
                                                }
                                    
                                    }
                                    else if($link_imagens[0]==1)
                                    {
                                        
                                    }
                                    if($link_video[0]!=1)
                                    {?>
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <iframe class="embed-responsive-item" <?php echo"src='".$link_video[0]."'";?> ></iframe>
                                        </div>
                                    <?php
                                        
                                    }
                                    else if($link_video[0]==1)
                                    {
                                        
                                    }
                                    
                                    ?>
                              </div>
                            <div class="card-footer bg-light pt-0">
                                <div class="row no-gutters font-weight-semi-bold text-center py-2 fs--1">
                                    <div class="col-auto">
                                        <button class="btn btn-light" onclick="curtir(this)" <?php echo"value='".$row['id_pubs']."'"; ?>>
                                            <img src="/assets/imgs/like-activo.png" alt="" width="20">
                                            <?php 
                                                $sql_curtidas="SELECT COUNT(*)FROM tb_curtir_post where id_post=".$row['id_pubs'];
                                                $res_curtidas=mysqli_query($linkdeconexao_db,$sql_curtidas);
                                                $curtidas=mysqli_num_rows($res_curtidas);
                                                ?>
                                                <span class="ml-1"><?php echo"".$curtidas." "; ?>Curtir</span></button> 
                                    </div>
                                </div>
                                  <?php 
                                    $sql_comentario_total="SELECT * FROM `tb_comentarios` WHERE `id_post=".$row['id_pubs'];
                                    $res_comentario_total=mysqli_query($linkdeconexao_db,$sql_comentario);
                                    
                                    $sql_comentario="SELECT * FROM `tb_comentarios` as c inner join tb_user as u on u.id=c.usuario where `id_post`=".$row['id_pubs']." Limit 3";
                                    $res_comentario=mysqli_query($linkdeconexao_db,$sql_comentario);
                                    while($row_comentario=mysqli_fetch_array($res_comentario))
                                    {
                                        ?>
                                    <div class="card mb-3">
                                        <div class="card-header bg-light">
                                            <div class="media">
                                            <div class="avatar avatar-2xl"> 
                                                <img class="rounded-circle" <?php echo"src='".converter_imagem($img)."'" ?> alt="" /> 
                                            </div>
                                            <div class="media-body align-self-center ml-2">
                                                <p class="mb-1 line-height-1"><a class="font-weight-semi-bold" href="#"><?php echo"".converter_usuario($row_comentario['usuario'])."" ?></a></p>
                                                <p class="mb-0 fs--1"><?php echo"".converter_data($row_comentario['data_hora']); ?></p>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="card-body overflow-hidden">
                                            <p> <?php echo"".$row_comentario['post'];?> </p>
                                        </div>    
                                    </div>
                                          <?php 
                                          if(mysqli_num_rows($res_comentario_total)>3){?>
                                                <div class="card mb-3">
                                                    <button onclick="comentario(this)" type="button" class="btn btn-light" onClick="Mais_comentario">Mostrar Mais Comentarios</button>
                                                </div>
                                    <?php }
                                    }
                                  ?>
                                  <div class="card mb-3">
                                        <div class="card-header bg-light">
                                            <div class="media">
                                            <div class="avatar avatar-2xl"> 
                                                <img class="rounded-circle" <?php echo"src='".converter_imagem($img)."'" ?> alt=""> </div>
                                            <div class="media-body align-self-center ml-2">
                                                <p class="mb-1 line-height-1"><a class="font-weight-semi-bold" href="#"><?php echo"".converter_usuario($row_comentario['usuario'])."" ?></a></p>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="card-body overflow-hidden">
                                              <form id="F_comentario" class="d-flex align-items-center border-top border-100 pt-3">
                                                <input name="texto" class="form-control rounded-capsule ml-2 fs--1" type="text" placeholder="Escreva um Comentário...">
                                                <button onclick="comentar(this)" type="submit" class="btn btn-light" href="#">
                                                        <img src="/assets/imgs/comment-activo.png" alt="" width="20"><span class="ml-1">Comentar</span>
                                                </button>
                                              </form>   
                                        </div>
                                </div>
                            </div>
                        </div>
              <?php }   
                }
            }
            else
            {
                echo"<div class='alert alert-danger' role='alert'>Texto Muito Curto</div>";
            }
        }
        else
        {
            echo"Erro";
        }
    }
    else
    {
        echo"<div class='alert alert-danger' role='alert'>Erro Token Invalido</div>";
    }
}
?>