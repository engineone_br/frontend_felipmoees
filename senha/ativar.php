<?php
include("../config/getdados.php");
if(isset($_GET['code']))
{
    $sql="select * from tb_user";
    $res=mysqli_query($linkdeconexao_db,$sql);
    while($row=mysqli_fetch_array($res))
    {
        if(md5($row['email'])==$_GET['code'])
        {
            $ok=true;
            $cod=$row['id'];
        }
    }
    if($ok==true)
    {
    ?>
    <!doctype html>
<html lang="pt-br">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="/img/icon.png">
<title>PrivêWS</title>

<!-- Principal CSS do Bootstrap -->
<link href="/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="/dist/css/dashboard.css" rel="stylesheet">
<link href="/dist/css/theme.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700%7cPoppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

<!-- Estilos customizados -->
<link href="/dist/css/dashboard.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.form.min.js"></script>
    <script type="text/javascript">
$(document).ready(function () {
    $('#btn_submit').click(function () {
        $('#senhaForm').ajaxForm({
                url: '/senha/atualizar.php',
                error: function (response, status, e) {
                    alert('Erro.');
                },
                complete: function (xhr) {
                        if(xhr.responseText=="ok")
                        {
                            $("#status").html("<div class='alert alert-success' role='alert'>Atualizada com Sucesso.</div>");
                            setTimeout(function() 
                            {
                                window.location.href = "/";
                            }, 1000); 
                        }
                        else
                        {
                            $("#status").html(xhr.responseText);
                        }
                        document.getElementById('password1').value=''; // Limpa o campo
                        document.getElementById('password2').value=''; // Limpa o campo
                }
            });
    });
});   
    </script>
</head>

<body class="text-center bg-gradient">
<!-- ===============================
			TROCAR A SENHA
		   =============================== --> 
<main class="main h-100 w-100">
  <div class="container w-100">
    <div class="align-items-center mt-4">
      <div class="col-sm-12 col-md-10 col-lg-8 mx-auto d-table h-100">
        <div class="d-table-cell align-middle">
          <div class="card">
            <div class="card-body">
                <div id="status"></div>
              <div class="m-sm-4">
                <div class="text-center">
					<h3><span class="fas fa-key text-danger"></span>
                        <p class="acesso">TROCAR A SENHA</p>
                    </h3>
                    <p>Informe A Sua Nova Senha </p>
					
                </div>
                <form id="senhaForm" method="post">
                    <input type="hidden" name="token" <?php echo"value='".$cod."'"; ?> >
                  <div class="form-group">
					  <p class="text-left">Nova Senha:</p>
                    <input id="password1" class="form-control form-control-lg" type="password" name="password1" placeholder="Informe sua nova senha">
                  </div>
                  <div class="form-group">
					  <p class="text-left">Nova Senha Novamente:</p>
                    <input id="password2" class="form-control form-control-lg" type="password" name="password2" placeholder="Informe sua nova senha novamente">
                  </div>
                  <div> </div>
                  <div class="card-body">
					<div class="row">
					  <div class="col-lg-4">
						<button id="btn_submit" type="submit" class="btn btn-danger btn-sm m-1 btn-block">Trocar</button>
					  </div>        
        			</div>  
					  
					   
					</div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
	<!-- ===============================
			FIM TROCAR A SENHA
		   =============================== --> 

<!-- Principal JavaScript do Bootstrap
    ================================================== --> 
<script src="/assets/js/vendor/popper.min.js"></script> 
<script src="/dist/js/bootstrap.min.js"></script> 
<script src="/dist/js/theme.js"></script> 
<script src="/@fortawesome/all.min.js"></script>
</body>
</html>

    <?php
    }
    else
    {
        echo"Codigo invalido";
    }
}
?>